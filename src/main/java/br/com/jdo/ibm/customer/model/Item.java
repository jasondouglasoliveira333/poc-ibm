package br.com.jdo.ibm.customer.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Item {

	@Id
	@GeneratedValue
	private Long id;
	
	@JsonProperty("codigo")
	private String code;
	@JsonProperty("produto")
	private String product;
	@JsonProperty("variedade")
	private String variety;
	@JsonProperty("pais")
	private String country;
	@JsonProperty("categoria")
	private String category;
	@JsonProperty("safra")
	private String vintage;
	@JsonProperty("preco")
	private BigDecimal price;
	
	
	@ManyToOne
	//To avoid json try to serialize
	@JsonIgnore
	private COrder order;


	@Override
	public String toString() {
		return "Item [id=" + id + ", code=" + code + ", product=" + product + ", variety=" + variety + ", country="
				+ country + ", category=" + category + ", vintage=" + vintage + ", price=" + price + "]";
	}
	
	
		
}
