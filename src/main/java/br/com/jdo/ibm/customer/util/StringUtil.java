package br.com.jdo.ibm.customer.util;

public class StringUtil {

	public static String adjustCpf(String custumerCnpj) {
		int idx = custumerCnpj.lastIndexOf(".");
		return custumerCnpj.substring(1,idx) + "-" + custumerCnpj.substring(idx+1);
	}

}
