package br.com.jdo.ibm.customer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.jdo.ibm.customer.model.Item;

@Repository
public interface ItenRepository extends JpaRepository<Item, Long>{
	
	@Query("select i.category from Item i group by i.category order by count(i.category) desc")
	List<String> findByGreatherSell();

	Item findFirstByCategory(String category);


}
