package br.com.jdo.ibm.customer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.jdo.ibm.customer.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

	@Query("select co.customer.id from COrder co group by co.customer.id order by sum(co.totalAmount) desc")
	List<Long> findByGreatherTotalAmount();

	//the method getReferenceById generate a problem in jackson serialization
	Customer findFirstById(Long id);

}
