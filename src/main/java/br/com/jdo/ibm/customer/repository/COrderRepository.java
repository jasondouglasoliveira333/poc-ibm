package br.com.jdo.ibm.customer.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.jdo.ibm.customer.model.COrder;

@Repository
public interface COrderRepository extends JpaRepository<COrder, Long> {

	COrder findFirstByDateBetweenOrderByTotalAmountDesc(Date dateStart, Date dateEnd);
	
}
