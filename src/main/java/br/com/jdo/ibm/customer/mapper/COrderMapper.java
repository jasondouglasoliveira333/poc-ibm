package br.com.jdo.ibm.customer.mapper;

import br.com.jdo.ibm.customer.dto.COrderDTO;
import br.com.jdo.ibm.customer.model.COrder;

public class COrderMapper {

	public static COrder map(COrderDTO cOrderDTO) {
		COrder cOrder = new COrder();
		cOrder.setCode(cOrderDTO.getCode());
		cOrder.setDate(cOrderDTO.getDate());
		cOrder.setTotalAmount(cOrderDTO.getTotalAmount());
		cOrder.setItens(cOrderDTO.getItens());
		return cOrder;
	}

}
