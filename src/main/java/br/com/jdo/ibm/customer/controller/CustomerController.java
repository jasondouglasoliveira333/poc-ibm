package br.com.jdo.ibm.customer.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.jdo.ibm.customer.dto.PageResponse;
import br.com.jdo.ibm.customer.model.COrder;
import br.com.jdo.ibm.customer.model.Customer;
import br.com.jdo.ibm.customer.model.Item;
import br.com.jdo.ibm.customer.repository.COrderRepository;
import br.com.jdo.ibm.customer.repository.CustomerRepository;
import br.com.jdo.ibm.customer.repository.ItenRepository;
import br.com.jdo.ibm.customer.util.DateUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin
@RequestMapping("customers")
@RestController
public class CustomerController {
	
	private static final Random RANDOM = new Random();

	@Autowired
	private COrderRepository cOrderRepository;
	
	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private ItenRepository itenRepository;
	


	@GetMapping("orders")
	@Transactional
	public ResponseEntity<?> consulta(@RequestParam(name="page", defaultValue = "0") Integer page, 
			@RequestParam(name="size", defaultValue="10000") Integer size, @RequestParam(name="property", defaultValue = "totalAmount") String property, 
			@RequestParam(name="direction", defaultValue = "ASC") String direction){
		try {
			PageRequest pR = PageRequest.of(page, size, Direction.fromString(direction), property);
			Page<COrder> pCorder = cOrderRepository.findAll(pR);
			PageResponse<COrder> cOrderResponse = new PageResponse<>();
			cOrderResponse.setContent(pCorder.getContent());
			cOrderResponse.setTotalPages(pCorder.getTotalPages());
			log.info("cOrderResponse:" + cOrderResponse);
			return ResponseEntity.ok(cOrderResponse);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}

	@GetMapping("orders/{year}/highest")
	@Transactional
	public ResponseEntity<?> highestBuys(@PathVariable String year){
		try {
			Date dateStart = DateUtil.parseYYYY(year);
			Date dateEnd = DateUtil.endOfYear(dateStart);
			log.info("dateStart:" + dateStart + " - dateEnd:" + dateEnd);
			COrder cOrder = cOrderRepository.findFirstByDateBetweenOrderByTotalAmountDesc(dateStart, dateEnd);
			log.info("cOrder:" + cOrder);
			return ResponseEntity.ok(cOrder);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}

	//I will stay ust with customer resource nane because we dont have another resource for the customer 
	@GetMapping("fidelity")
	@Transactional
	public ResponseEntity<?> fidelity(){
		try {
			List<Customer> customerList = new ArrayList<>();
			List<Long> customerIds = customerRepository.findByGreatherTotalAmount();
			for (int x=0; x < 3; x++) {
				//the method getReferenceById generate a problem in jackson serialization
				customerList.add(customerRepository.findFirstById(customerIds.get(x)));
			}
			PageResponse<Customer> cOrderResponse = new PageResponse<>();
			cOrderResponse.setContent(customerList);
			cOrderResponse.setTotalPages(1);
			log.info("cOrderResponse:" + cOrderResponse);
			return ResponseEntity.ok(cOrderResponse);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}

	@GetMapping("recommentations")
	@Transactional
	public ResponseEntity<?> recommentations(){
		try {
			List<String> itensCategories = itenRepository.findByGreatherSell();
			int MAX = Math.min(itensCategories.size(), 10);
			itensCategories = itensCategories.subList(0, MAX);
			int itemSelected = RANDOM.nextInt(MAX);
			Item iten = itenRepository.findFirstByCategory(itensCategories.get(itemSelected));
			return ResponseEntity.ok(iten);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().body(null);
		}
	}


}
