package br.com.jdo.ibm.customer.dto;

import java.util.List;

import lombok.Data;

@Data
public class PageResponse<T> {
	private Integer  totalPages;
	private List<T> content;
	
}
