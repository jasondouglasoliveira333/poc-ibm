package br.com.jdo.ibm.customer.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Data
public class Customer {
	
	@Id  
	@GeneratedValue
	private Long id;

	@JsonProperty("nome")
	private String name;
	
	@JsonProperty("cpf")
	private String cpf;

}
