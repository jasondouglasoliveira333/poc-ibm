package br.com.jdo.ibm.customer.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Entity
@Data
@ToString
public class COrder {

	@Id
	@GeneratedValue
	private Long id;
	
	@JsonProperty("codigo")
	private String code;
	@JsonProperty("data")
	@JsonFormat(shape = Shape.STRING, pattern = "dd-MM-yyyy")
	private Date date;
	@JsonProperty("valorTotal")
	private BigDecimal totalAmount;
	
	@ManyToOne
	@JsonProperty("cliente")
	private Customer customer;
	
	@OneToMany(mappedBy = "order")
	private List<Item> itens;
	
}
