package br.com.jdo.ibm.customer.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.jdo.ibm.customer.model.Item;
import lombok.Data;

@Data
public class COrderDTO {

	private Long id;
	
	@JsonProperty("codigo")
	private String code;
	@JsonProperty("data")
	@JsonFormat(shape = Shape.STRING, pattern = "dd-MM-yyyy")
	private Date date;
	@JsonProperty("valorTotal")
	private BigDecimal totalAmount;
	@JsonProperty("cliente")
	private String custumerCnpj;
	
	@OneToMany(mappedBy = "order")
	private List<Item> itens;
	
}
