package br.com.jdo.ibm.customer;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import br.com.jdo.ibm.customer.dto.COrderDTO;
import br.com.jdo.ibm.customer.mapper.COrderMapper;
import br.com.jdo.ibm.customer.model.COrder;
import br.com.jdo.ibm.customer.model.Customer;
import br.com.jdo.ibm.customer.repository.COrderRepository;
import br.com.jdo.ibm.customer.repository.CustomerRepository;
import br.com.jdo.ibm.customer.repository.ItenRepository;
import br.com.jdo.ibm.customer.util.StringUtil;

@SpringBootTest
public class LoadOrdersTests {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private COrderRepository cOrderRepository;

	@Autowired
	private ItenRepository itenRepository;

	@Autowired
	private ObjectMapper objectMapper;
	
	//Tests to load the database
//	@Test
	@Rollback(false)
	@Transactional
	public void loadOrders() {
		try {
			TimeZone.setDefault(TimeZone.getTimeZone("GMT-0"));

			Map<String, Customer> customerMap = new HashMap<>();
			byte[] data = Files.readAllBytes(Paths.get(".\\src\\test\\resources\\598b16291100004705515ec5.json"));
			ObjectReader or = objectMapper.readerForListOf(Customer.class);
			List<Customer> cList = or.readValue(data);
			cList.stream().forEach(customer -> {
				customerRepository.save(customer);
				customerMap.put(customer.getCpf(), customer);
			});
			
			data = Files.readAllBytes(Paths.get(".\\src\\test\\resources\\598b16861100004905515ec7.json"));
			or = objectMapper.readerForListOf(COrderDTO.class);
			List<COrderDTO> cOrderList = or.readValue(data);
			cOrderList.stream().forEach(cOrderDTO -> {
				COrder cOrder = COrderMapper.map(cOrderDTO);
				String adjustCpj = StringUtil.adjustCpf(cOrderDTO.getCustumerCnpj());
				System.out.println("adjustCpj:" + adjustCpj);
				cOrder.setCustomer(customerMap.get(adjustCpj));
				cOrderRepository.save(cOrder);
				cOrder.getItens().stream().forEach(item -> {
					item.setOrder(cOrder);
					itenRepository.save(item);
				});
			});
		}catch (Exception e) {
			e.printStackTrace();
		}

	}
}
