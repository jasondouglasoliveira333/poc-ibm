package br.com.jdo.ibm.customer;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;

import br.com.jdo.ibm.customer.model.COrder;
import br.com.jdo.ibm.customer.repository.COrderRepository;
import br.com.jdo.ibm.customer.repository.CustomerRepository;
import br.com.jdo.ibm.customer.repository.ItenRepository;
import br.com.jdo.ibm.customer.util.DateUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
public class CustomerServicesTests {

	@Autowired
	private COrderRepository cOrderRepository;
	
	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private ItenRepository itenRepository;

	@Test
	public void testOrdersOrderedByTotalAmountAsc() {
		PageRequest pR = PageRequest.of(0, 10, Direction.fromString("ASC"), "totalAmount");
		Page<COrder> pCorder = cOrderRepository.findAll(pR);
		assert(pCorder.getSize() == 10);
	}
	
	@Test
	public void testGrestestOrder2016() throws Exception {
		String year = "2016";
		Date dateStart = DateUtil.parseYYYY(year);
		Date dateEnd = DateUtil.endOfYear(dateStart);
		log.info("dateStart:" + dateStart + " - dateEnd:" + dateEnd);
		COrder cOrder = cOrderRepository.findFirstByDateBetweenOrderByTotalAmountDesc(dateStart, dateEnd);
		assert(cOrder != null);
	}

	//Covering just the main method
	@Test
	public void testGrestestCustomers() throws Exception {
		List<Long> customerIds = customerRepository.findByGreatherTotalAmount();
		assert(customerIds.size() > 5);
	}
	
	//Covering just the main method
	@Test
	public void testGrestestMoreSelled() throws Exception {
		List<String> itensCategories = itenRepository.findByGreatherSell();
		assert(itensCategories.size() > 2);
	}


}
